<?php

use App\Models\Movie;
use App\Models\User;
use Inertia\Testing\AssertableInertia as Assert;

test('should access to movies page', function () {
    $user = User::factory()->create();
    $response = $this
        ->actingAs($user)
        ->get('/movies');

    $response->assertOk();
});

test('should be redirect to login if not connected', function () {
    $response = $this->get('/movies');

    $response->assertRedirect(route('login', absolute: false));
});

test('should view all my movies', function () {
    $user = User::factory()->create();
    Movie::factory(count: 3)->create();
    $movie1 = Movie::factory()->create();
    $user->movies()->attach($movie1);
    $user->save();
    $this
        ->actingAs($user)
        ->get('/movies')
        ->assertInertia(fn (Assert $page) => $page
            ->component('Movie/Index')
            ->has('movies', 1)
        );
});
