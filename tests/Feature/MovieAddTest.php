<?php

use App\Models\User;

test('cannot add if not logged in', function () {
    $response = $this->post('movie/add', [
        [
            'id' => 111111,
            'title' => 'Film test 1',
            'poster_path' => 'https://image.tmdb.org/t/p/original/aaaaaaaaaaaaaa.jpg',
        ],
        [
            'id' => 111112,
            'title' => 'Film test 2',
            'poster_path' => 'https://image.tmdb.org/t/p/original/aaaaaaaaaaaaab.jpg',
        ]
    ]);

    $response->assertStatus(302);
    $response->assertRedirect('/login');
});

test('adding one movie', function () {
    $user = User::factory()->create();

    $this->actingAs($user);

    $response = $this->post(route('movie.add'), ['movies' => [
        [
            'id' => 111111,
            'title' => 'Film test 1',
            'poster_path' => 'https://image.tmdb.org/t/p/original/aaaaaaaaaaaaaa.jpg',
        ]
    ]]);

    $response->assertStatus(302);
    $response->assertRedirect(route('movie.search'));
    $this->assertDatabaseHas('movies', [
        'external_id' => 111111,
        'title' => 'Film test 1',
        'poster_path' => 'https://image.tmdb.org/t/p/original/aaaaaaaaaaaaaa.jpg',
    ]);
    $response
        ->assertSessionHas('message', '1 film a été ajouté')
        ->assertSessionHas('type', 'success');
});

test('adding multiple movies', function () {
    $user = User::factory()->create();

    $this->actingAs($user);

    $response = $this->post(route('movie.add'), ['movies' => [
        [
            'id' => 111111,
            'title' => 'Film test 1',
            'poster_path' => 'https://image.tmdb.org/t/p/original/aaaaaaaaaaaaaa.jpg',
        ],
        [
            'id' => 111112,
            'title' => 'Film test 2',
            'poster_path' => 'https://image.tmdb.org/t/p/original/aaaaaaaaaaaaab.jpg',
        ],
        [
            'id' => 111113,
            'title' => 'Film test 3',
            'poster_path' => 'https://image.tmdb.org/t/p/original/aaaaaaaaaaaaac.jpg',
        ]
    ]]);

    $response->assertStatus(302);
    $response->assertRedirect(route('movie.search'));
    $this->assertDatabaseHas('movies', [
        'external_id' => 111111,
        'title' => 'Film test 1',
        'poster_path' => 'https://image.tmdb.org/t/p/original/aaaaaaaaaaaaaa.jpg',
    ]);
    $this->assertDatabaseHas('movies', [
        'external_id' => 111112,
        'title' => 'Film test 2',
        'poster_path' => 'https://image.tmdb.org/t/p/original/aaaaaaaaaaaaab.jpg',
    ]);
    $this->assertDatabaseHas('movies', [
        'external_id' => 111113,
        'title' => 'Film test 3',
        'poster_path' => 'https://image.tmdb.org/t/p/original/aaaaaaaaaaaaac.jpg',
    ]);
    $response
        ->assertSessionHas('message', '3 films ont été ajoutés')
        ->assertSessionHas('type', 'success');
});
