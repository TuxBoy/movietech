<?php

return [
    App\Providers\AppServiceProvider::class,
    App\Providers\RepositoryProvider::class,
    App\Providers\TmdbRepositoryProvider::class,
    App\Providers\TmdbServiceProvider::class,
];
