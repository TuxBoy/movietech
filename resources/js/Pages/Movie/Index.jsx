import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';

import {Head} from "@inertiajs/react";

export default function Index({auth, movies}) {
    return (
        <AuthenticatedLayout
            user={auth.user}
            header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Mes films</h2>}
        >
            <Head title="Mes films"/>

            <div className='container mx-auto m-4'>
                <div className='flex flex-wrap justify-center items-end gap-10'>
                    {Object.entries(movies).map(([key, movie]) => (
                        <div className='shadow-2xl relative rounded-3xl' key={key}>
                            {movie.poster_path !== 'https://image.tmdb.org/t/p/original' ? (
                                <img className={'rounded-3xl'} width={200} src={movie.poster_path} alt={movie.title}/>
                            ) : (
                                <div
                                    className='max-w-[200px] text-center text-slate-50 bg-blue-600 rounded-3xl pt-[6px]'>
                                    <span>{movie.title}</span>
                                    <img width={200}
                                         src='/images/image_non_disponible.png'
                                         alt={movie.title}
                                         className={'rounded-3xl'}
                                    />
                                </div>
                            )}
                        </div>
                    ))}
                </div>
            </div>
        </AuthenticatedLayout>
    )
}
