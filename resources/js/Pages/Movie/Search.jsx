import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import SearchForm from './Partials/SearchForm';

import {Head, useForm} from '@inertiajs/react';
import {useState} from 'react';
import SecondaryButton from "@/Components/SecondaryButton.jsx";

export default function Search({auth, movies}) {
  const [moviesToAdd, setMoviesToAdd] = useState([])

  const searchForm = useForm({
    movies: []
  })

  const handleCheckboxChange = (movie) => {
    if (!moviesToAdd.includes(movie)) {
      const updatedMoviesToAdd = [...moviesToAdd, movie]
      setMoviesToAdd(updatedMoviesToAdd)
      searchForm.setData('movies', updatedMoviesToAdd)
    } else {
      const updatedMoviesToAdd = moviesToAdd.filter((id) => id !== movie)
      setMoviesToAdd(updatedMoviesToAdd)
      searchForm.setData('movies', updatedMoviesToAdd)
    }
  }

  const addMovies = () => {
    searchForm.post(route('movie.add'), {
      preserveScroll: true,
      onSuccess: () => {
        searchForm.reset()
        setMoviesToAdd([])
      },
      onError: (errors) => {
        alert(errors.movies)
      }
    })
  }
  return (
    <AuthenticatedLayout
      user={auth.user}
      header={<h2 className='font-semibold text-xl text-gray-800 leading-tight'>Rechercher un film</h2>}
    >
      <Head title='Rechercher un film'/>
      <div className='container mx-auto'>
        <div className='flex flex-col flex-wrap md:flex-row justify-center gap-4 m-4'>
          <SearchForm/>
          {moviesToAdd.length > 0 && (
            <SecondaryButton onClick={addMovies}>
              {moviesToAdd.length === 1 ? `Ajouter 1 film` : `Ajouter ${moviesToAdd.length} films`}
            </SecondaryButton>
          )}
        </div>
        <div className='flex flex-wrap justify-center items-end gap-10'>
          {Object.entries(movies).map(([key, movie]) => (
            <div className='shadow-2xl relative rounded-3xl' key={key}>
              {movie.poster_path !== 'https://image.tmdb.org/t/p/original' ? (
                <img className={'rounded-3xl'} width={200} src={movie.poster_path} alt={movie.title}/>
              ) : (
                <div className='max-w-[200px] text-center text-slate-50 bg-blue-600 rounded-3xl pt-[6px]'>
                  <span>{movie.title}</span>
                  <img width={200}
                       src='/images/image_non_disponible.png'
                       alt={movie.title}
                       className={'rounded-3xl'}
                  />
                </div>
              )}
              <div className='absolute transform -translate-y-1/2 flex justify-around p-4 w-full'>
                <button
                  type='button'
                  className='shadow-xl bg-blue-500 text-white rounded-full w-9 h-9 flex items-center justify-center m-1'
                >
                  i
                </button>
                <button
                  id={movie.id}
                  onClick={() => handleCheckboxChange(movie)}
                  type='button'
                  className={'shadow-xl rounded-full w-9 h-9 flex items-center justify-center m-1 transition ' +
                    (moviesToAdd.includes(movie) ? 'bg-green-500 text-white' : 'bg-yellow-500 text-white')}
                >
                  {moviesToAdd.includes(movie) ? '-' : '+'}
                </button>
                <button
                  type='button'
                  className='shadow-xl bg-red-500 text-white rounded-full w-9 h-9 flex items-center justify-center m-1'
                >
                  ♡
                </button>
              </div>
            </div>
          ))}
        </div>
      </div>
    </AuthenticatedLayout>
  )
}
