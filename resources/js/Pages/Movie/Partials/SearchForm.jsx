import {useRef} from "react";
import {useForm} from "@inertiajs/react";
import PrimaryButton from "@/Components/PrimaryButton.jsx";

export default function SearchForm({className = ''}) {
    const searchMovieInput = useRef()

    const searchForm = useForm({
        query: ''
    })

    const searchMovie = () => {
        searchForm.get(route('movie.search'), {
            onSuccess: () => searchForm.reset(),
            onError: (errors) => {
                console.error(errors)
            }
        })
    }

    return (
        <div className={'flex flex-col gap-4 md:flex-row ' + className}>
            <div className={"flex flex-col"}>
                <label
                    htmlFor="query"
                    className={"font-medium text-sm text-black"}
                >
                    Titre du film
                </label>
                <input
                    id="query"
                    type="text"
                    className={"border-gray-300 focus:border-blue-600 focus:ring-blue-600 rounded-md shadow-sm"}
                    ref={searchMovieInput}
                    value={searchForm.data.query}
                    onChange={(event) => searchForm.setData('query', event.target.value)}
                />
            </div>
            <PrimaryButton
                onClick={searchMovie}
                disabled={searchForm.processing}
            >
                Rechercher
            </PrimaryButton>
        </div>
    )

}
