import {Link, Head} from '@inertiajs/react';

export default function Welcome({auth}) {
  return (
    <>
      <Head title="Movie Library - Votre bibliothèque de films"/>
      <div className="bg-slate-50 text-gray-600">
        <div className="relative min-h-screen flex flex-col items-center justify-center selection:bg-blue-600 selection:text-white">
          <div className="relative w-full max-w-2xl px-6 lg:max-w-7xl">
            <main className="mt-10">
              <div className="grid grid-cols-1 gap-6 md:grid-cols-2">
                <div className="flex items-center">
                  <img
                    src="/images/movie-library.png"
                    alt="Movie Library"
                    className="w-full h-auto"
                  />
                </div>

                <div className="flex flex-col justify-center">
                  <h1 className="text-4xl font-bold text-blue-600">
                    Bienvenue !
                  </h1>
                  <p className="mt-4 text-lg text-gray-600">
                    Votre bibliothèque de films, où que vous soyez !<br/>
                    Scannez votre dvd, blu-ray, blu-ray 4k, et ajoutez le à votre bibliothèque.<br/>
                    Partagez votre collection avec vos amis. <br/>
                    Ajoutez des films à votre liste d'envie et bien plus...
                  </p>
                  <div className="mt-6 flex">
                    {auth.user ? (
                      <Link
                        href={route('dashboard')}
                        className="px-4 py-2 bg-blue-600 text-white rounded-lg shadow hover:bg-blue-700 focus:outline focus:outline-2 focus:rounded-sm focus:outline-blue-600"
                      >
                        Mon tableau de bord
                      </Link>
                    ) : (
                      <>
                        <Link
                          href={route('login')}
                          className="px-4 py-2 bg-blue-600 text-white rounded-lg shadow hover:bg-blue-700 focus:outline focus:outline-2 focus:rounded-sm focus:outline-blue-600"
                        >
                          Se connecter
                        </Link>
                        <Link
                          href={route('register')}
                          className="ml-4 px-4 py-2 bg-blue-600 text-white rounded-lg shadow hover:bg-blue-700 focus:outline focus:outline-2 focus:rounded-sm focus:outline-blue-600"
                        >
                          S'inscrire
                        </Link>
                      </>
                    )}
                  </div>
                </div>
              </div>
            </main>
          </div>
        </div>
      </div>
    </>
  )
}
