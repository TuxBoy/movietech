export default function ApplicationLogo(props) {
  return (
    <img {...props} src={"/images/movie-library.png"} alt={"Logo Movie Library"}/>
  );
}
