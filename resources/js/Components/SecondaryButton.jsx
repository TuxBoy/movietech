export default function SecondaryButton({className = '', disabled, children, ...props}) {
  return (
    <button
      {...props}
      className={
        `px-4 py-2 ring-1 ring-blue-600 bg-white text-blue-600 rounded-md font-semibold text-xs uppercase tracking-widest
        hover:ring-2 hover:ring-blue-700 hover:text-blue-700
        focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 focus:outline-none
        active:bg-blue-700 active:text-white
        transition ${disabled && 'opacity-25'} ` + className
      }
      disabled={disabled}
    >
      {children}
    </button>
  );
}
