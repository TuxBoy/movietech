export default function PrimaryButton({className = '', disabled, children, ...props}) {
  return (
    <button
      {...props}
      className={
        `px-4 py-2 ring-1 ring-blue-600 bg-blue-600 rounded-md font-semibold text-xs text-slate-50 uppercase tracking-widest
        hover:bg-blue-700 hover:ring-2 hover:ring-blue-700 hover:outline-none
        focus:bg-blue-600 focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 focus:outline-none
        active:bg-gray-900
        transition ${disabled && 'opacity-25'} ` + className
      }
      disabled={disabled}
    >
      {children}
    </button>
  );
}
