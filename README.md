<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## Starting the project

Ensure you have composer, make and docker installed

You can launch this command to see the different commands available for the project. (Makefile)

    make help

<b>OPTIONAL</b> : For more simplicity you can add this line in your .bashrc

    alias sail='./vendor/bin/sail'

Launch this command

    sail artisan key:generate

Fill your env variables:

    TMDB_API_KEY

Launch this command to start everything
    
    make start


You have now access to http://localhost

## Jeu de données

    make seed

This will create a user : 

<b>admin@email.com / password</b>
