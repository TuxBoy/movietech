ifndef CI_JOB_ID
	GREEN  := $(shell tput -Txterm setaf 2)
	YELLOW := $(shell tput -Txterm setaf 3)
	RESET  := $(shell tput -Txterm sgr0)
	TARGET_MAX_CHAR_NUM=30
endif

.DEFAULT_GOAL := help
.PHONY: help
help: ## Show this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: start
start: ## Start containers
	@if [ ! -f .env ]; then cp .env.example .env; fi
	composer install && \
	./vendor/bin/sail up -d && \
	./vendor/bin/sail yarn && \
	./vendor/bin/sail yarn dev

.PHONY: stop
stop: ## Stop containers
	./vendor/bin/sail down

.PHONY: migrate
migrate: ## Migrate migrations
	./vendor/bin/sail php artisan migrate

.PHONY: refresh-database
refresh-database: ## Refresh the database
	./vendor/bin/sail php artisan migrate:fresh

.PHONY: seed
seed: ## Seed
	./vendor/bin/sail php artisan db:seed

.PHONY: test
test: ## Run tests
	./vendor/bin/sail php ./vendor/bin/pest

.PHONY: analyse
analyse: ## Run code analyse
	./vendor/bin/sail composer audit && \
	./vendor/bin/sail php ./vendor/bin/phpstan analyse && \
	./vendor/bin/sail php ./vendor/bin/pint -v --test

.PHONY: pint-fix
pint-fix: ## Pint fix
	./vendor/bin/sail php ./vendor/bin/pint -v

.PHONY: pipeline
pipeline: ## Run before push to know if pipeline will fail
	$(MAKE) test
	$(MAKE) analyse
