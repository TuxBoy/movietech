<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Translation\PotentiallyTranslatedString;

class ValidMovieData implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param Closure(string): PotentiallyTranslatedString $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (!is_array($value)) {
            $fail('The :attribute must be an array.');
        }

        if (!isset($value['id']) || !is_int($value['id'])) {
            $fail('The :attribute must have a valid id.');
        }

        if (!isset($value['title']) || !is_string($value['title'])) {
            $fail('The :attribute must have a valid title.');
        }

        if (!isset($value['poster_path']) || !is_string($value['poster_path'])) {
            $fail('The :attribute must have a valid poster.');
        }
    }
}
