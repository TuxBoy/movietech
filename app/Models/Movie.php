<?php

namespace App\Models;

use App\Models\Pivot\MovieUser;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Movie extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'external_id',
        'title',
        'poster_path',
    ];

    public function users(): BelongsToMany
    {
        return $this
            ->belongsToMany(User::class)
            ->withPivot(['view', 'support_type', 'favorite', 'toView'])
            ->using(MovieUser::class);
    }

    public function posterPath(): Attribute
    {
        return Attribute::make(
            get: fn (string $value) => 'https://image.tmdb.org/t/p/original/' . $value,
        );
    }
}
