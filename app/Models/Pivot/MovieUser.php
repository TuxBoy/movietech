<?php

declare(strict_types=1);

namespace App\Models\Pivot;

use App\Enum\SupportType;
use Illuminate\Database\Eloquent\Relations\Pivot;

class MovieUser extends Pivot
{
    protected function casts(): array
    {
        return [
            'favorite' => 'boolean',
            'view' => 'boolean',
            'toView' => 'boolean',
            'support_type' => SupportType::class,
        ];
    }
}
