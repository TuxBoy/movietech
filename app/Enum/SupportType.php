<?php

declare(strict_types=1);

namespace App\Enum;

enum SupportType: string
{
    case DVD = 'dvd';
    case BLURAY = 'Blu-ray';
    case STEELBOOK = 'steelbook';
    case FOURK = '4K';
}
