<?php

declare(strict_types=1);

namespace App\Providers;

use App\Repository\Tmdb\Movie\MovieRepositoryInterface;
use App\Repository\Tmdb\Movie\TmdbMovieRepository;
use App\Repository\Tmdb\Search\SearchRepositoryInterface;
use App\Repository\Tmdb\Search\TmdbSearchRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Tmdb\Client;
use Tmdb\Repository\MovieRepository as TmdbBaseMovieRepository;
use Tmdb\Repository\SearchRepository as TmdbBaseSearchRepository;

final class TmdbRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(MovieRepositoryInterface::class, function (Application $app) {
            return new TmdbMovieRepository(repository: new TmdbBaseMovieRepository(client: $app->get(Client::class)));
        });

        $this->app->bind(SearchRepositoryInterface::class, function (Application $app) {
            return new TmdbSearchRepository(repository: new TmdbBaseSearchRepository(client: $app->get(Client::class)));
        });
    }
}
