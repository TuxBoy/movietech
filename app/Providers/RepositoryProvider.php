<?php

namespace App\Providers;

use App\Repository\Movie\MovieRepository;
use App\Repository\Movie\MovieRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(MovieRepositoryInterface::class, MovieRepository::class);
    }
}
