<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Tmdb\Client;
use Tmdb\Event\BeforeRequestEvent;
use Tmdb\Event\Listener\Request\ApiTokenRequestListener;
use Tmdb\Event\Listener\RequestListener;
use Tmdb\Event\RequestEvent;
use Tmdb\Token\Api\ApiToken;

final class TmdbServiceProvider extends ServiceProvider
{
    private const CONFIG_KEY = 'tmdb';

    public function register(): void
    {
        $this->setupConfiguration();

        $this->app->singleton(Client::class, function (Application $app) {
            $config = $app['config']['tmdb'];
            $options['api_token'] = new ApiToken($config['api_key']);
            $eventDispatcher = new EventDispatcher();
            $options['event_dispatcher'] = [
                'adapter' => $eventDispatcher,
            ];

            $client = new Client($options);
            $requestListener = new RequestListener($client->getHttpClient(), $eventDispatcher);
            $eventDispatcher->addListener(RequestEvent::class, $requestListener);
            $apiTokenListener = new ApiTokenRequestListener($client->getToken());
            $eventDispatcher->addListener(BeforeRequestEvent::class, $apiTokenListener);

            return $client;
        });
    }

    private function setupConfiguration(): void
    {
        $config = $this->defaultConfig();
        $this->mergeConfigFrom($config, self::CONFIG_KEY);
    }

    /**
     * Returns the default configuration path
     */
    private function defaultConfig(): string
    {
        return $this->app->configPath('tmdb.php');
    }

    public function provides(): array
    {
        return ['tmdb'];
    }
}
