<?php

declare(strict_types=1);

namespace App\Transformer;

use App\Dto\MovieData;
use Tmdb\Model\Movie;

final class SearchMovieTransformer
{
    public function transformToDto(Movie $movie): MovieData
    {
        return new MovieData(
            title: $movie->getTitle(),
            id: $movie->getId(),
            poster_path: 'https://image.tmdb.org/t/p/original' . $movie->getPosterPath()
        );
    }

    /**
     * @return MovieData[]
     */
    public function transformCollection(array $movies): array
    {
        return array_map([$this, 'transformToDto'], $movies);
    }
}
