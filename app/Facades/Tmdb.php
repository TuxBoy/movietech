<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;
use Tmdb\Client;

final class Tmdb extends Facade
{
    /**
     * Get the registered name of the component.
     */
    protected static function getFacadeAccessor(): string
    {
        return Client::class;
    }
}
