<?php

declare(strict_types=1);

namespace App\Dto;

final readonly class MovieData
{
    public function __construct(
        public string $title,
        public int $id,
        public string $poster_path,
    ) {}
}
