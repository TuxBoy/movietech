<?php

namespace App\Http\Requests;

use App\Rules\ValidMovieData;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class AddMoviesRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'movies' => ['required', 'array', 'min:1'],
            'movies.*' => ['required', new ValidMovieData()],
        ];
    }
}
