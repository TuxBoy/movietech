<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Dto\MovieData;
use App\Repository\Tmdb\Movie\MovieRepositoryInterface;
use App\Repository\Tmdb\Search\SearchRepositoryInterface;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

final class SearchController extends Controller
{
    public function __construct(
        private readonly SearchRepositoryInterface $searchRepository,
        private readonly MovieRepositoryInterface $movieRepository,
    ) {}

    public function search(Request $request): Response
    {
        $query = $request->get('query');

        $search = $query ? $this->searchRepository->search(title: $query) : $this->movieRepository->getPopular();
        $movies = array_map(fn (MovieData $movie) => $movie, $search);

        return Inertia::render('Movie/Search', [
            'movies' => $movies,
        ]);
    }
}
