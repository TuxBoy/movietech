<?php

namespace App\Http\Controllers;

use App\Dto\MovieData;
use App\Http\Requests\AddMoviesRequest;
use App\Models\User;
use App\UseCase\AddMovies;
use Illuminate\Auth\AuthManager;
use Illuminate\Http\RedirectResponse;
use Inertia\Inertia;
use Inertia\Response;

final class MovieController extends Controller
{
    public function __construct(private readonly AuthManager $auth) {}

    public function add(AddMoviesRequest $request, AddMovies $useCase): RedirectResponse
    {
        $moviesToAdd = array_map(function (array $movie) {
            return new MovieData(
                title: $movie['title'],
                id: $movie['id'],
                poster_path: $movie['poster_path']
            );
        }, $request->validated('movies'));

        $movies = $useCase($moviesToAdd);

        $message = '1 film a été ajouté';
        if (count($movies) > 1) {
            $message = sprintf('%d films ont été ajoutés', count($movies));
        }

        return redirect(route('movie.search', absolute: false))
            ->with([
                'message' => $message,
                'type' => 'success',
            ]);
    }

    public function index(): Response
    {
        /** @var User $user */
        $user = $this->auth->user();

        return Inertia::render('Movie/Index', ['movies' => $user->movies()->get()]);
    }
}
