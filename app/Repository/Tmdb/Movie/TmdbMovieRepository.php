<?php

namespace App\Repository\Tmdb\Movie;

use App\Repository\Tmdb\AbstractMovieRepository;
use App\Transformer\SearchMovieTransformer;
use Tmdb\Model\Movie;
use Tmdb\Repository\MovieRepository as TmdbBaseMovieRepository;

/**
 * @method TmdbBaseMovieRepository getRepository()
 */
final class TmdbMovieRepository extends AbstractMovieRepository implements MovieRepositoryInterface
{
    public function find(int $id): Movie
    {
        /** @var Movie $movie */
        $movie = $this->getRepository()->load($id);

        return $movie;
    }

    /**
     * @inheritDoc
     */
    public function getPopular(): array
    {
        $resultCollection = $this->getRepository()->getPopular(['language' => 'fr']);

        return $this->getTransformer()->transformCollection($resultCollection->toArray());
    }

    public function getTransformer(): SearchMovieTransformer
    {
        return new SearchMovieTransformer();
    }
}
