<?php

namespace App\Repository\Tmdb\Movie;

use App\Dto\MovieData;
use Tmdb\Model\Movie;

interface MovieRepositoryInterface
{
    public function find(int $id): Movie;

    /**
     * @return MovieData[]
     */
    public function getPopular(): array;
}
