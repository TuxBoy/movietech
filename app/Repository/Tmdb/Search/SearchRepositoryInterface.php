<?php

declare(strict_types=1);

namespace App\Repository\Tmdb\Search;

use Tmdb\Repository\SearchRepository as TmdbSearchRepository;

/**
 * @method TmdbSearchRepository getRepository()
 */
interface SearchRepositoryInterface
{
    public function search(string $title, int $page = 1): array;
}
