<?php

namespace App\Repository\Tmdb\Search;

use App\Dto\MovieData;
use App\Repository\Tmdb\AbstractMovieRepository;
use App\Transformer\SearchMovieTransformer;
use Tmdb\Model\Collection\ResultCollection;
use Tmdb\Model\Search\SearchQuery\MovieSearchQuery;
use Tmdb\Repository\SearchRepository as TmdbBaseSearchRepository;

/**
 * @method TmdbBaseSearchRepository getRepository()
 */
final class TmdbSearchRepository extends AbstractMovieRepository implements SearchRepositoryInterface
{
    /**
     * @return MovieData[]
     */
    public function search(string $title, int $page = 1): array
    {
        $query = new MovieSearchQuery();
        $query->page($page);
        $query->language('fr');

        /** @var ResultCollection $find */
        $find = $this->getRepository()->searchMovie(query: $title, parameters: $query);

        return $this->getTransformer()->transformCollection($find->toArray());
    }

    public function getTransformer(): SearchMovieTransformer
    {
        return new SearchMovieTransformer();
    }
}
