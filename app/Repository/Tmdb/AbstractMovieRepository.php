<?php

namespace App\Repository\Tmdb;

use Tmdb\Api\Movies;
use Tmdb\Repository\AbstractRepository;

abstract class AbstractMovieRepository
{
    public function __construct(private readonly AbstractRepository $repository) {}

    protected function getMoviesApi(): Movies
    {
        return $this->getRepository()->getClient()->getMoviesApi();
    }

    public function getRepository(): AbstractRepository
    {
        return $this->repository;
    }
}
