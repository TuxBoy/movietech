<?php

declare(strict_types=1);

namespace App\Repository\Movie;

use App\Models\Movie;

final class MovieRepository implements MovieRepositoryInterface
{
    public function getMovieByExternalId(int $externalId): ?Movie
    {
        /** @var Movie|null $movie */
        $movie = Movie::where('external_id', $externalId)->first();

        return $movie;
    }
}
