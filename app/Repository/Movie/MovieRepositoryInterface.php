<?php

declare(strict_types=1);

namespace App\Repository\Movie;

use App\Models\Movie;

interface MovieRepositoryInterface
{
    public function getMovieByExternalId(int $externalId): ?Movie;
}
