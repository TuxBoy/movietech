<?php

declare(strict_types=1);

namespace App\UseCase;

use App\Dto\MovieData;
use App\Models\Movie;
use App\Models\User;
use App\Repository\Movie\MovieRepositoryInterface;
use Illuminate\Auth\AuthManager;

final readonly class AddMovies
{
    public function __construct(private AuthManager $authManager, private MovieRepositoryInterface $movieRepository) {}

    /**
     * @param MovieData[] $tmdbMovies
     * @return Movie[]
     */
    public function __invoke(array $tmdbMovies): array
    {
        $movies = [];
        foreach ($tmdbMovies as $tmdbMovie) {
            $movie = $this->saveOrUpdate(tmdbMovie: $tmdbMovie);

            /** @var User $user */
            $user = $this->authManager->user();
            if ($user->movies()->where('movie_id', $movie->id)->doesntExist()) {
                $user->movies()->save($movie);
            }

            $movies[] = $movie;
        }

        return $movies;
    }

    private function saveOrUpdate(MovieData $tmdbMovie): Movie
    {
        $movie = $this->movieRepository->getMovieByExternalId(externalId: $tmdbMovie->id) ?? new Movie();

        $movie->external_id = $tmdbMovie->id;
        $movie->title = $tmdbMovie->title;
        $movie->poster_path = $tmdbMovie->poster_path;
        $movie->save();

        return $movie;
    }
}
